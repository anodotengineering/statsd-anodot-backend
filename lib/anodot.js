
/*
 * Flush stats to anodot.
 *
 * To enable this backend, include 'statsd-anodot-backend' in the backends
 * configuration array:
 *
 *   backends: ['statsd-anodot-backend']
 *
 * This backend supports the following config options:
 *
 *   anodotApiToken: Your Anodot API key
 *   anodotApiHost: Your Anodot API Host e.g. http://10.0.0.8:8080
 */

var net = require('net'),
    util = require('util'),
    os = require('os'),
    http = require('http'),
    https = require('https'),
    url = require('url');

var prefixCounter = '.count',
    prefixRate = '.rate',
    prefixGauge = '',
    prefixSet = '',
    prefixStat = '',
    prefixBin = '',
    prefixTimerUnit = '',
    prefixHost = '';

var Anodot = function(startup_time, config, events) {

    var self = this;
    this.api_token = config.anodotApiToken;
    this.api_host = config.anodotApiHost || 'http://api.anodot.com';
    this.metricFormat = config.anodotMetricFormat || '1.0';
    this.addHostPrefix = config.anodotAddHostProperty || false;
    this.pendingQueue = config.anodotPendingQueue || 300;
    this.debug = config.debug || false;
    this.flushInterval = config.flushInterval;
    this.sendCounterAsRate = (config.anodotSendCounterAsRate === undefined) ? true : config.anodotSendCounterAsRate;
    this.anodotStats = [];
    this.anodotStats['droppedSamples'] = 0;
    this.anodotStats['pending_requests'] = 0;
    this.anodotStats['anodot_api_failures'] = 0;
    this.anodotStats['last_flush'] = startup_time;
    this.anodotStats['last_exception'] = startup_time;

    events.on('flush', function(timestamp, metrics) { self.flush(timestamp, metrics); });
    events.on('status', function(callback) { self.status(callback); });
    if (this.metricFormat === '2.0') {
        prefixCounter = '.target_type=count';
        prefixRate = '.target_type=rate';
        prefixGauge = '.target_type=gauge';
        prefixSet = '.target_type=set';
        prefixStat = 'stat=';
        prefixBin = 'bin=';
        prefixTimerUnit = '.unit=ms';
    }

    if (this.addHostPrefix === true) {
        if (this.metricFormat === '2.0') {
            prefixHost = 'host=' + os.hostname() + '.';
        }else {
            prefixHost = os.hostname() + '.';
        }
    }

    if (( config.anodotMetricsAllow !== undefined) && (config.anodotMetricsAllow !== '')) {
        this.metricsAllow = new RegExp(config.anodotMetricsAllow, "i");
    }

    if (( config.anodotMetricsDeny !== undefined) && (config.anodotMetricsDeny !== '')){
        this.metricsDeny = new RegExp(config.anodotMetricsDeny, "i");
    }

};

Anodot.prototype.filterMetric = function(key) {
    var self = this;

    if (self.metricsAllow !== undefined) { // if allow rule defined and key is not returned from allow rule dump this key
        if ( key.search(self.metricsAllow) === -1)
            return true;
    }

    if (self.metricsDeny !== undefined) { // if deny rule defined and key is returned from deny rule dump this metric
        if ( key.search(self.metricsDeny) !== -1)
            return true;
    }

    return false;
};

Anodot.prototype.post_stats = function(message) {
    var self = this;
    var body = JSON.stringify(message);
    var req;
    var transport, api_port, api_host;
    var parsed = url.parse(this.api_host);



    if(self.anodotStats['pending_requests'] > self.pendingQueue){
        self.anodotStats['droppedSamples'] += 1;
        return;
    }


    if (parsed.protocol === 'http:') {
        transport = http;
        api_port = parsed.port || 80;
    } else {
        transport = https;
        api_port = parsed.port || 443;
    }

    api_host = parsed.hostname;

    req = transport.request({
            host: api_host,
            port: api_port,
            path: '/api/v1/metrics' + '?token=' + self.api_token,
            method: 'POST',
            headers: {
                "Host": api_host,
                "Content-Length": body.length,
                "Content-Type": "application/json"
            }
        },
        function(response) {
            self.anodotStats['pending_requests'] -= 1;
            self.anodotStats['last_flush'] = Math.round(new Date().getTime() / 1000);
            response.resume();
        });

    req.on('error', function(e) {
        util.log('Skipping, cannot send data to anodot: ' + e.message);
        self.anodotStats['anodot_api_failures'] += 1;
        self.anodotStats['pending_requests'] -= 1;
    });

    self.anodotStats['pending_requests'] += 1;
    if ( this.debug ){
        console.log('sending: ' + body);
    }
    req.write(body);
    req.end();
};
Anodot.prototype.flush = function (ts, metrics) {
    var counters = metrics.counters;
    var gauges = metrics.gauges;
    var sets = metrics.sets;
    var timers = metrics.timers;
    var timer_data = metrics.timer_data;
    var pctThreshold = metrics.pctThreshold;
    var self = this;
    var metricPrefix = 'anodot_host=' + os.hostname() + '.anodot_comp=statsd.';
    var payload = [];
    var value;
    var key;

    // Send counters
    for (key in counters) {
        value = counters[key];
        var valuePerSecond = value / (this.flushInterval / 1000); // calculate "per second" rate

        if (this.filterMetric(key) === true) continue;

        if ( this.sendCounterAsRate === true){
            payload.push({
                name: prefixHost + key + prefixRate,
                timestamp: ts,
                value: valuePerSecond
            });
        }

        payload.push({
            name: prefixHost + key + prefixCounter,
            timestamp: ts,
            value: value,
            tags:{target_type:'counter'}
        });
    }

    // Send gauges
    for (key in gauges) {
        value = gauges[key];

        if (this.filterMetric(key) === true) continue;

        payload.push({
            name: prefixHost + key + prefixGauge,
            timestamp: ts,
            value: value
        });
    }

    // Send sets
    for (key in sets) {
        value = sets[key].values().length;

        if (this.filterMetric(key) === true) continue;

        payload.push({
            name: prefixHost + key + prefixCounter,
            timestamp: ts,
            value: value,
            tags:{target_type:'counter'}
        });
    }

    // send timers
    for (key in timer_data) {
        for (var timer_data_key in timer_data[key]) {
            if (this.filterMetric(key + '.' + timer_data_key) === true) continue;
            if (typeof(timer_data[key][timer_data_key]) === 'number') {
                if ( (timer_data_key === 'count') || (timer_data_key === 'count_90') ||
                     (timer_data_key === 'sum') || (timer_data_key === 'sum_90') ||
                     (timer_data_key === 'sum_squares') || (timer_data_key === 'sum_squares_90')
                   ) {
                    payload.push({name: prefixHost + key + '.' + prefixStat + timer_data_key + prefixTimerUnit + prefixCounter, timestamp: ts, value: timer_data[key][timer_data_key], tags:{target_type:'counter'} });
                } else {
                    payload.push({name: prefixHost + key + '.' + prefixStat + timer_data_key + prefixTimerUnit + prefixGauge, timestamp: ts, value: timer_data[key][timer_data_key] });
                }
            } else {
                for (var timer_data_sub_key in timer_data[key][timer_data_key]) {
                    payload.push({name: prefixHost + key + '.' + prefixStat + timer_data_key + '.' + prefixBin + timer_data_sub_key + prefixTimerUnit + prefixGauge, timestamp: ts, value: timer_data[key][timer_data_key][timer_data_sub_key] });
                }
            }
        }
    }

    // add anodot metrics
    payload.push({'name': metricPrefix + 'what=api_errors' , 'timestamp': ts, 'value': self.anodotStats['anodot_api_failures'] });
    payload.push({'name': metricPrefix + 'what=uptime' , 'timestamp': ts, 'value': process.uptime() });
    payload.push({'name': metricPrefix + 'what=pending_requests' , 'timestamp': ts, 'value': self.anodotStats['pending_requests'] });
    payload.push({'name': metricPrefix + 'what=droppedSamples' , 'timestamp': ts, 'value': self.anodotStats['droppedSamples'] });

    this.post_stats(payload);
};

Anodot.prototype.status = function (writeCb) {
    var stat;

    for (stat in this.anodotStats) {
        writeCb(null, 'anodot', stat, this.anodotStats[stat]);
    }
};

exports.init = function (startup_time, config, events) {
    var instance = new Anodot(startup_time, config, events);
    return true;
};
