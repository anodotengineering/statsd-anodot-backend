# statsd-anodot-backend

A plugin to connect etsy's statsD to Anodot

## Installation

    $ cd /path/to/statsd-dir
    $ npm install statsd-anodot-backend --save
    
## Configuration

```js
anodotApiToken: "your_api_token"
anodotApiHost: "anodot_api_host"
anodotMetricFormat: "2.0" | "1.0" (default "1.0")
anodotAddHostProperty: true | false (default false)
anodotSendCounterAsRate: true | false (default true)
anodotMetricsAllow: "key1|key2" optional - allow metrics containing keys key1 or key2. value can be a js regex
anodotMetricsDeny: "key3|key4" optional - deny metrics containing keys key1 or key2. value can be a js regex
Note: if both Allow and Deny rules exist, allow will run first and deny will run on the outcome of allow
```

## How to enable
Add statsd-anodot-backend to your list of statsd backends:

```js
backends: ["statsd-anodot-backend"]
```

